import XMonad
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.StatusBar (defToggleStrutsKey, statusBarProp, withEasySB)
import XMonad.Hooks.StatusBar.PP
import XMonad.Layout.Spacing (spacingWithEdge)
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Loggers (logTitles)
import XMonad.Util.SpawnOnce (spawnOnce)


import qualified Data.Map as M

myBar = "xmobar ~/.config/xmobar/xmobarrc"
myTerminal = "alacritty"
myGap = 5
myWorkspaces = map show [1..9]
myFocusFollowsMouse = True
myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = magenta " • "
    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = wrap " " "" . xmobarBorder "Top" "#8be9fd" 2
    , ppHidden          = white . wrap " " ""
    , ppHiddenNoWindows = lowWhite . wrap " " ""
    , ppUrgent          = red . wrap (yellow "!") (yellow "!")
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white    "[") (white    "]") . magenta . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30
    blue, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#ff79c6" ""
    blue     = xmobarColor "#bd93f9" ""
    white    = xmobarColor "#f8f8f2" ""
    yellow   = xmobarColor "#f1fa8c" ""
    red      = xmobarColor "#ff5555" ""
    lowWhite = xmobarColor "#bbbbbb" ""
myLayoutHook = tiled ||| Mirror tiled ||| Full
  where
    tiled   = Tall nmaster delta ratio
    nmaster = 1      -- Default number of windows in the main pane
    ratio   = 54/100 -- Default proportion of screen occupied by main pane
    delta   = 3/100  -- Percent of screen to increment by when resizing panes
myStartupHook = do
  spawnOnce "lxsession"
  spawnOnce "picom"
  spawnOnce "dunst"
  spawnOnce "nitrogen --restore"
  spawnOnce "volumeicon"
  spawnOnce "trayer --edge top --align right --height 22 --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 128"
myConfig = def
  { terminal = myTerminal
  , workspaces = myWorkspaces
  , focusFollowsMouse = myFocusFollowsMouse
  , layoutHook = spacingWithEdge myGap $ myLayoutHook
  , startupHook = myStartupHook
  }
  `additionalKeysP`
  [ ("<XF86AudioMute>", spawn "amixer set Master toggle")
  , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
  , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
  ]

main :: IO ()
main = xmonad
     . ewmhFullscreen
     . ewmh
     . withEasySB (statusBarProp myBar (pure myXmobarPP)) defToggleStrutsKey
     $ myConfig
