#!/bin/bash

set -eou pipefail

user="$(id -nu)"

# Install bashrc
cp /home/"${user}"/.config/bash/bashrc /home/"${user}"/.bashrc

# Create .xinitrc
{
  printf 'xinput set-prop "SYNA30BB:00 06CB:CE07 Touchpad" 342 1\n'
  printf 'xinput set-prop "SYNA30BB:00 06CB:CE07 Mouse" 315 1\n'
  printf 'exec /home/%s/.cabal/bin/xmonad\n' "${user}"
} > /home/"${user}"/.xinitrc

# Install distro packages
sudo pacman -Syy --noconfirm \
  xorg-server \
  xorg-apps \
  xorg-xinit \
  xorg-xmessage \
  libx11 \
  libxft \
  libxinerama \
  libxrandr \
  libxss \
  pkgconf \
  noto-fonts-emoji \
  ttf-firacode-nerd \
  picom \
  nitrogen \
  dmenu \
  dunst \
  lxsession-gtk3 \
  trayer-srg \
  xfce4-power-manager \
  alsa-utils \
  volumeicon \
  alacritty \
  firefox

# Install ghcup
curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
source /home/"${user}"/.ghcup/env

# Install KMonad and enable it as a service
git clone https://github.com/kmonad/kmonad /tmp/kmonad && cd $_
stack build
sed -i -E "s#input \(device-file\s.*\)#input (device-file \"$(find /dev/input/by-path -name '*-kbd' | head -n 1)\")#" /home/"${user}"/.config/kmonad/config.kbd
sudo find /tmp/kmonad/.stack-work/install/ -type f -name kmonad -exec cp {} /usr/bin/kmonad \;
sed -i -E "s:^(ExecStart=).*:\1/usr/bin/kmonad /home/"${user}"/.config/kmonad/config.kbd:" /tmp/kmonad/startup/kmonad@.service
sudo cp /tmp/kmonad/startup/kmonad@.service /usr/lib/systemd/system/kmonad.service
sudo systemctl enable kmonad
sudo systemctl start kmonad

# Install Starship
curl -sS https://starship.rs/install.sh | sh
printf '\neval "$(starship init bash)"\n' >> /home/"${user}"/.bashrc

# Install xmonad
git clone https://github.com/xmonad/xmonad /home/"${user}"/.config/xmonad/xmonad
git clone https://github.com/xmonad/xmonad-contrib /home/"${user}"/.config/xmonad/xmonad-contrib
printf 'packages: */*.cabal\n' > /home/"${user}"/.config/xmonad/cabal.project
cabal update
cabal install --package-env=/home/"${user}"/.config/xmonad --lib xmonad xmonad-contrib
cabal install --package-env=/home/"${user}"/.config/xmonad xmonad

# Install xmobar
cabal install xmobar -fall_extensions
